package exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ClientErrorException extends RuntimeException {

    private HttpStatus status;

    public ClientErrorException(String s, HttpStatus status) {

        super(s);
        this.status = status;
    }
}
