package com.example.user.service;


import com.example.user.model.User;
import com.example.user.repository.UserRepository;
import exception.ClientErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User getUserById(String id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ClientErrorException("User not found", HttpStatus.NOT_FOUND));
        return user;
    }

    public User updateUser(String userId, User updatePayload) {
        User existedUser = getUserById(userId);
        if (updatePayload.getFullname() != null) {
            existedUser.setFullname(updatePayload.getFullname());
        }
        userRepository.save(existedUser);
        return existedUser;
    }
}
