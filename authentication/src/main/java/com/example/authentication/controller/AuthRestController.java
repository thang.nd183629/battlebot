package com.example.authentication.controller;

import com.example.authentication.dto.LoginRequest;
import com.example.authentication.dto.SignupRequest;
import com.example.authentication.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AuthRestController {

    @Autowired
    private AuthService authService;

    @PostMapping("/auth/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginReq) {
        return new ResponseEntity<>(authService.login(loginReq), HttpStatus.OK);
    }

    @PostMapping("/auth/register")
    public ResponseEntity<?> register(@RequestBody SignupRequest signupReq) {
        authService.register(signupReq);
        return new ResponseEntity<String>("Registered", HttpStatus.OK);
    }

}
