package com.example.authentication.service;

import com.example.authentication.dto.LoginRequest;
import com.example.authentication.dto.LoginResponse;
import com.example.authentication.dto.SignupRequest;
import com.example.authentication.dto.UpdateTokenDTO;
import com.example.authentication.model.User;
import com.example.authentication.repository.UserRepository;
import com.example.authentication.util.JwtUtil;
import com.example.authentication.util.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private ObjectMapper SERIALIZER;
    @Autowired
    private RestTemplate restTemplate;
    @Value("${robot.service.url}")
    private String ROBOT_SERVICE_URL;

    public LoginResponse login(LoginRequest loginRequest) {
        User user = userRepository.findByUsername(loginRequest.getUsername());
        if (user == null) throw new RuntimeException("Username not found");
        if (!user.getPassword().equals(Utils.MD5(loginRequest.getPassword()))) {
            throw new RuntimeException("Invl credential");
        }
        String token = jwtUtil.generateToken(user.getId());
        return new LoginResponse(token, user);
    }

    public void register(SignupRequest signupRequest) {

        User user = userRepository.findByUsername(signupRequest.getUsername());
        if (user != null) throw new RuntimeException("Duplicate username");
        System.out.println(signupRequest);
        user = new User(null, signupRequest.getUsername(),
                Utils.MD5(signupRequest.getPassword())
                , signupRequest.getFullname());
        userRepository.save(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("user-id", user.getId());
        HttpEntity<String> entity;
        try {
            entity = new HttpEntity<String>(
                    SERIALIZER.writeValueAsString(new UpdateTokenDTO(10000L))
                    , headers);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        restTemplate.exchange(String.format("%s/user/token", ROBOT_SERVICE_URL), HttpMethod.POST, entity, Object.class);
    }


}
