package com.example.authentication.repository;


import com.example.authentication.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    User findByUsername(String username);

}
