package com.example.battle.controller;

import com.example.battle.common.Response;
import com.example.battle.service.BattleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/battle")
@Slf4j
public class RealtimeBattleController {
    @Autowired
    private BattleService battleService;

    @GetMapping("/{id}/attack")
    public ResponseEntity<?> attack(@RequestHeader(name = "user-id") String userId, @PathVariable(name = "id") String battleId) {

        return ResponseEntity.status(202).body( battleService.playByTurn(battleId, userId));

    }

    @GetMapping("/init")
    public ResponseEntity<?> init(@RequestHeader(name = "user-id") String userId, @RequestParam("hero-id") String heroId) {

        return

                ResponseEntity.status(HttpStatus.OK).body(battleService.init(userId, heroId));
    }

    @GetMapping("/turn-status/{id}")
    public ResponseEntity<?> getStatus(@PathVariable String id, @RequestHeader(name = "user-id") String userid) {
        return ResponseEntity.ok(new Response<>(battleService.getTurnStatus(id)));
    }

    @GetMapping("/turn/{id}")
    public ResponseEntity<?> getTurnResult(@PathVariable String id, @RequestHeader(name = "user-id") String userid) {
        return ResponseEntity.ok(new Response<>(battleService.getTurnResult(id)));
    }


}
