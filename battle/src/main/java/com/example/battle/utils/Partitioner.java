package com.example.battle.utils;

public class Partitioner {
    public static int partition(String id, int partitions) {
        int sum = 0;
        for (char ch : id.toCharArray()) {

            sum += ch - '0';

        }
        return sum % partitions;
    }


}
