package com.example.battle.config;

import com.example.battle.rabbitmq.consumer.RabbitConsumerThread;
import com.example.battle.rabbitmq.consumer.RabbitTurnConsumerThread;
import liquibase.pro.packaged.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ApplicationConfig {

    private final ExecutorService executorService = Executors.newFixedThreadPool(5);


    private final List<RabbitConsumerThread<?>> consumers = new ArrayList<>();
    @Value("#{'${rabbit.turn.queue}'.split(',')}")
    private List<String> queues;
    @Autowired
    private ApplicationContext context;


    @PostConstruct
    public void initConsumerThread() {
        AutowireCapableBeanFactory factory = context.getAutowireCapableBeanFactory();
        for (String queue : queues) {
            RabbitConsumerThread consumerThread = new RabbitTurnConsumerThread(queue);
            consumers.add(consumerThread);
            factory.autowireBean(consumerThread);
            factory.initializeBean(consumerThread, queue);
        }


        for (RabbitConsumerThread consumerThread : consumers) {
            executorService.submit(consumerThread);
        }
    }


}
