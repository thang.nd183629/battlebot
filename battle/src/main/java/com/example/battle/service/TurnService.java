package com.example.battle.service;

import com.example.battle.domain.RobotDomainModel;
import com.example.battle.domain.RobotMapperService;
import com.example.battle.dto.TurnMessage;
import com.example.battle.model.Battle;
import com.example.battle.model.RobotState;
import com.example.battle.model.Turn;
import com.example.battle.redis.ICache;
import com.example.battle.repository.BattleRepository;
import com.example.battle.repository.RoboStateRepository;
import com.example.battle.repository.TurnRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

@Service
@Slf4j
public class TurnService {
    @Autowired
    private TurnRepository turnRepository;
    @Autowired
    private BattleRepository battleRepository;
    @Autowired
    private RoboStateRepository roboStateRepository;
    @Autowired
    private RobotMapperService mapperService;
    @Autowired
    private BattleService battleService;


    @Autowired
    @Qualifier("jedis")
    private ICache jedisCache;

    @Transactional

    public void processTurn(TurnMessage turnMessage) {
        try {
            Turn turn = turnRepository.findById(turnMessage.getId()).orElseThrow(() -> new RuntimeException("Corrupted queue"));
            String battleId = turn.getBattle_id();
            Battle battle = battleRepository.findById(battleId).orElseThrow(() -> new RuntimeException("Corrupted queue"));
            if (battle.getIs_ended()) {
                log.info("Battle already ended");
                return;
            }
            Turn prevTurn = turnRepository.findTurnByTurn_no(turn.getTurn_no() - 1, battleId);
            RobotState player = roboStateRepository.findById(prevTurn.getPlayer_state()).orElseThrow(() -> new RuntimeException("Corrupted queue"));
            RobotState bot = roboStateRepository.findById(prevTurn.getBot_state()).orElseThrow(() -> new RuntimeException("Corrupted queue"));
            RobotDomainModel playerModel = mapperService.modelToDomain(player);
            RobotDomainModel botModel = mapperService.modelToDomain(bot);


            if (!turnMessage.getUser_id().equals("0")) {

                playerModel.attack(botModel);

            } else {
                botModel.attack(playerModel);

            }

            RobotState playerState = mapperService.domainToModel(playerModel);
            RobotState botState = mapperService.domainToModel(botModel);
            roboStateRepository.saveAll(Arrays.asList(playerState, botState));
            turn.setPlayer_state(playerState.getId());
            turn.setBot_state(botState.getId());
            turnRepository.save(turn);

            if (botModel.getHp() == 0) {
                battleService.endBattle(battle, turnMessage.getUser_id());
            }
            if (playerModel.getHp() == 0) {
                battleService.endBattle(battle, "0");
            }

            TimerTask task = new TimerTask() {
                public void run() {
                    jedisCache.set(turn.getId(), "true", null);

                }
            };
            Timer timer = new Timer("Timer");

            long delay = 10000L;
            timer.schedule(task, delay);

        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }

    }


}
