package com.example.battle.service;

import com.example.battle.domain.RobotMapperService;
import com.example.battle.dto.*;
import com.example.battle.model.Battle;
import com.example.battle.model.RobotState;
import com.example.battle.model.Scene;
import com.example.battle.model.Turn;
import com.example.battle.rabbitmq.producer.TurnProducer;
import com.example.battle.redis.ICache;
import com.example.battle.repository.BattleRepository;
import com.example.battle.repository.RoboStateRepository;
import com.example.battle.repository.SceneRepository;
import com.example.battle.repository.TurnRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import exception.ClientErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class BattleService {
    @Autowired
    private TurnProducer turnProducer;

    @Autowired
    private BattleRepository battleRepository;
    @Autowired
    private RoboStateRepository roboStateRepository;
    @Autowired
    private SceneRepository sceneRepository;

    @Autowired
    private RobotMapperService mapperService;
    @Autowired
    private TurnRepository turnRepository;
    @Autowired
    private ObjectMapper SERIALIZER;
    @Autowired
    @Qualifier("jedis")
    private ICache jedisCache;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private SimpMessagingTemplate template;

    @Value("${robot.service.url}")
    private String ROBOT_SERVICE_URL;
    private Integer BASE_STAT_INCR = 10;
    private Integer VICTORY_REWARD = 100;
    private Integer DEFEATED_REWARD = 50;

    @Transactional
    public BattleDTO init(String userId, String chosenRobotId) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("user-id", userId);
        final HttpEntity<String> entity = new HttpEntity<String>(headers);
        Robot[] roboList;
        try {
            log.info(ROBOT_SERVICE_URL);
            roboList = restTemplate.exchange(String.format("%s/robot", ROBOT_SERVICE_URL), HttpMethod.GET, entity, Robot[].class).getBody();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("server err");
        }

        assert roboList != null;
        Robot random = roboList[(new Random()).nextInt(roboList.length)];
        Robot player;

        try {
            player = restTemplate.exchange(String.format("%s/user/robot/%s", ROBOT_SERVICE_URL, chosenRobotId), HttpMethod.GET, entity, Robot.class).getBody();

        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().is4xxClientError()) {
                throw new ClientErrorException("Robot not found", HttpStatus.NOT_FOUND);
            } else throw e;

        } catch (Exception e) {
            throw new RuntimeException("server err");
        }


        Scene latestScene = sceneRepository.findLatestScene(userId);
        if (latestScene == null) {
            latestScene = new Scene();
            latestScene.setLevel(0);
            latestScene.setUser_id(userId);
            sceneRepository.save(latestScene);
        }
        //build bot

        Battle battle = new Battle();
        battle.setHero_id(player.getId());
        battle.setScene_id(latestScene.getId());
        battle.setOpponent_id(random.getId());
        battleRepository.save(battle);

        RobotState initialBotState = mapperService.modelToState(random);


        initialBotState.setAtk(initialBotState.getAtk() + BASE_STAT_INCR * latestScene.getLevel());
        initialBotState.setHp(initialBotState.getHp() + BASE_STAT_INCR * latestScene.getLevel());


        RobotState initialPlayerState = mapperService.modelToState(player);


        roboStateRepository.saveAll(Arrays.asList(initialPlayerState, initialBotState));

        Turn turn = new Turn();
        turn.setBattle_id(battle.getId());
        turn.setBot_state(initialBotState.getId());
        turn.setPlayer_state(initialPlayerState.getId());
        turn.setTurn_no(0);

        turnRepository.save(turn);
        return new BattleDTO(battle.getId(), turn2dto(turn));


    }


    public TurnPending playByTurn(String battleId, String userId) {
        Battle battle = battleRepository.findById(battleId).orElseThrow(() -> new ClientErrorException("Battle not found", HttpStatus.NOT_FOUND));
        if (battle.getIs_ended()) {
            throw new ClientErrorException("Battle ended", HttpStatus.ACCEPTED);
        }
        Turn prevTurn = turnRepository.findLatestTurn(battleId);
        Turn playerTurn = new Turn();
        playerTurn.setBattle_id(battleId);
        playerTurn.setTurn_no(prevTurn.getTurn_no() + 1);
        Turn botTurn = new Turn();
        botTurn.setBattle_id(battleId);
        botTurn.setTurn_no(prevTurn.getTurn_no() + 2);
        turnRepository.saveAll(Arrays.asList(playerTurn, botTurn));
        turnProducer.pushMessage(new TurnMessage(userId, playerTurn.getId(), battleId));
        turnProducer.pushMessage(new TurnMessage("0", botTurn.getId(), battleId));
        jedisCache.set(botTurn.getId(), "false", null);
        jedisCache.set(playerTurn.getId(), "false", null);
        return new TurnPending(playerTurn.getId(), botTurn.getId());
    }

    public void endBattle(Battle battle, String winner) {
        int turnCounts = (int) Math.ceil(turnRepository.getTurnCount(battle.getId()) / 2);
        Long rewardTokens = 0L;
        if (winner.equals("0")) {
            rewardTokens += turnCounts * DEFEATED_REWARD;
        } else {
            rewardTokens += turnCounts * VICTORY_REWARD;
            Scene scene = sceneRepository.findById(battle.getScene_id()).orElseThrow(() -> new RuntimeException("Corrupted db"));
            scene.setIs_completed(true);
            Scene unlockedScene = new Scene(null, winner, scene.getLevel() + 1, false);
            sceneRepository.saveAll(Arrays.asList(scene, unlockedScene));
        }
        battle.setIs_ended(true);
        battle.setWinner(winner);
        battleRepository.save(battle);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("user-id", winner);
        HttpEntity<String> entity;
        try {
            entity = new HttpEntity<String>(
                    SERIALIZER.writeValueAsString(new UpdateTokenDTO(rewardTokens))
                    , headers);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        Token token = restTemplate.exchange(String.format("%s/user/token", ROBOT_SERVICE_URL), HttpMethod.POST, entity, Token.class).getBody();
        template.convertAndSend("/topic/user",

                BattleResultDTO.builder().battleId(battle.getId()).turns(getBattleScript(battle)).winner(winner).reward(rewardTokens).build());

    }

    private List<TurnDTO> getBattleScript(Battle battle) {
        String battleId = battle.getId();
        List<Turn> turns = turnRepository.getTurnByBattle(battleId);
        List<TurnDTO> turnDTOS = new ArrayList<>();
        for (Turn turn : turns) {
            turnDTOS.add(turn2dto(turn));
        }

        return turnDTOS;

    }

    public String getTurnStatus(String id) {
        String turnStatus = jedisCache.get(id);
        return turnStatus;
    }

    public TurnDTO getTurnResult(String id) {
        Turn turn = turnRepository.findById(id).orElseThrow(() -> new ClientErrorException("Turn not found", HttpStatus.NOT_FOUND));
        return turn2dto(turn);
    }

    private TurnDTO turn2dto(Turn turn) {
        TurnDTO turnDTO = new TurnDTO();
        turnDTO.setBattle_id(turn.getBattle_id());
        turnDTO.setTurn_no(turn.getTurn_no());
        RobotState botState = roboStateRepository.findById(turn.getBot_state()).orElseThrow(() -> new RuntimeException("Corrupted db"));
        RobotState playerState = roboStateRepository.findById(turn.getPlayer_state()).orElseThrow(() -> new RuntimeException("Corrupted db"));
        turnDTO.setBot_state(botState);
        turnDTO.setPlayer_state(playerState);
        return turnDTO;
    }


}
