package com.example.battle.repository;

import com.example.battle.model.RobotState;
import org.springframework.data.repository.CrudRepository;

public interface RoboStateRepository extends CrudRepository<RobotState, String> {

}
