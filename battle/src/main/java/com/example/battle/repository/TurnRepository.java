package com.example.battle.repository;

import com.example.battle.model.Turn;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TurnRepository extends CrudRepository<Turn, String> {
    @Query(value = "select t from Turn t where t.turn_no = :turn_no and t.battle_id = :battleId")
    Turn findTurnByTurn_no(Integer turn_no, String battleId);

    @Query(value = "select * from turn where turn.battle_id = :battleId order by turn.turn_no desc limit 1", nativeQuery = true)
    Turn findLatestTurn(String battleId);

    @Query(value = "select count(*) from turn where turn.battle_id = :battleId and turn.turn_no != 0", nativeQuery = true)
    Integer getTurnCount(String battleId);
    @Query(value = "select t from Turn t where t.battle_id = :battleId and t.bot_state is not null and t.player_state is not null ")
    List<Turn> getTurnByBattle(String battleId);
}
