package com.example.battle.repository;

import com.example.battle.model.Scene;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SceneRepository extends CrudRepository<Scene, String> {
    @Query(nativeQuery = true, value = "select * from scene where user_id = :userId and is_completed is not true order by level desc limit 1 ")
    Scene findLatestScene(String userId);
}
