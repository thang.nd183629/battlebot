package com.example.battle.repository;


import com.example.battle.model.Battle;
import org.springframework.data.repository.CrudRepository;

public interface BattleRepository extends CrudRepository<Battle, String> {
}
