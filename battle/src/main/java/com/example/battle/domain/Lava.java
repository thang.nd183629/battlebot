package com.example.battle.domain;

public class Lava extends RobotDomainModel {
    @Override
    public void attack(RobotDomainModel opponent) {
        if (opponent.getHp() - this.getAtk() <= 0) {
            opponent.setHp(0);
            return;
        }
        opponent.setHp(opponent.getHp() - this.getAtk());
    }
}
