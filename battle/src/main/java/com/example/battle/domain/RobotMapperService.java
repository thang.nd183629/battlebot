package com.example.battle.domain;

import com.example.battle.dto.Robot;
import com.example.battle.model.RoboType;
import com.example.battle.model.RobotState;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RobotMapperService {
    @Autowired
    private ObjectMapper SERIALIZER;

    public RobotDomainModel modelToDomain(RobotState robot) {
        RobotDomainModel model;
        switch (robot.getType()) {
            case LAVA:
                model = new Lava();
                break;
            case OBLIX:
                model = new Oblix();
                break;

            case RAIDER:
                model = new Raider();
                break;
            default:
                throw new RuntimeException("Robot type not specified");
        }
        model.setAtk(robot.getAtk());
        model.setHp(robot.getHp());
        return model;
    }

    public RobotState domainToModel(RobotDomainModel robotDomainModel) {
        RobotState robotState = SERIALIZER.convertValue(robotDomainModel, RobotState.class);
        if (robotDomainModel instanceof Lava) {
            robotState.setType(RoboType.LAVA);
        } else if (robotDomainModel instanceof Oblix) {
            robotState.setType(RoboType.OBLIX);

        } else if (robotDomainModel instanceof Raider) {
            robotState.setType(RoboType.RAIDER);
        } else {
            throw new RuntimeException("Robot type not specified");
        }
        return robotState;

    }

    public RobotState modelToState(Robot robot) {
        RobotState state = SERIALIZER.convertValue(robot, RobotState.class);
        state.setId(null);
        return state;
    }

}
