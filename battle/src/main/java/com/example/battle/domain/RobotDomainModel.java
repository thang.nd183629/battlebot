package com.example.battle.domain;

import com.example.battle.model.RoboType;
import lombok.Data;
import lombok.Setter;

@Setter
@Data
public abstract class RobotDomainModel {
    private String id;
    private Integer hp;
    private Integer atk;
    public abstract void attack(RobotDomainModel opponent);
}
