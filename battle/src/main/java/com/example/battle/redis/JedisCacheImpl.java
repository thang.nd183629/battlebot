package com.example.battle.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

@Component("jedis")
public class JedisCacheImpl implements ICache {
    private JedisPool connectionPool;


    public JedisCacheImpl(@Value("${redis.port}")
                          Integer port, @Value("${redis.host}")
                          String host) {
        JedisPoolConfig poolConfig = new JedisPoolConfig();

        poolConfig.setMaxTotal(10000);
        poolConfig.setMaxIdle(500);

        poolConfig.setTestWhileIdle(true);
        poolConfig.setTestOnBorrow(false);
        poolConfig.setTestOnReturn(false);

        connectionPool = new JedisPool(poolConfig, host, port, Protocol.DEFAULT_TIMEOUT);

    }


    @Override
    public Boolean set(String key, String value, Integer expired) {
        try (Jedis jedis = connectionPool.getResource()) {
            if (expired != null) {
                jedis.setex(key, expired, value);
            }
            jedis.set(key, value);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean del(String key) {
        try (Jedis jedis = connectionPool.getResource()) {
            jedis.del(key);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public String get(String key) {
        try (Jedis jedis = connectionPool.getResource()) {
            return jedis.get(key);
        } catch (Exception e) {
            return null;
        }

    }

}
