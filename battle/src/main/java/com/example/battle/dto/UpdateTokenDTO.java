package com.example.battle.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateTokenDTO {
    private Long amount;

}
