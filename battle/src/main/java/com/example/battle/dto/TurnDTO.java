package com.example.battle.dto;


import com.example.battle.model.RobotState;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data


public class TurnDTO {
    private String id;
    private String battle_id;
    private Integer turn_no;
    private RobotState bot_state;
    private RobotState player_state;


}
