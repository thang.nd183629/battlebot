package com.example.battle.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BattleDTO {
    private String battleId;
    @JsonProperty("turn")
    private TurnDTO turnDTO;

}
