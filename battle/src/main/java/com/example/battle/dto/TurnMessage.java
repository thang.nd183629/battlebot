package com.example.battle.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnMessage {
    private String user_id; //0 for bot
    private String id;
    private String battle_id;


}
