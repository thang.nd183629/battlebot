package com.example.battle.dto;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TurnPending {
    public String player_turn;
    public String bot_turn;
}
