package com.example.battle.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BattleResultDTO {
    private String battleId;
    private List<TurnDTO> turns;
    private String winner;
    private Long reward;
}
