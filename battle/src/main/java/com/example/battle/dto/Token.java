package com.example.battle.dto;


import lombok.Getter;

import lombok.Setter;

@Getter
@Setter
public class Token {
    private String id;
    private String userId;
    private Long amount;
}
