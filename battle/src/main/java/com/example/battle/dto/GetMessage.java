package com.example.battle.dto;

import lombok.Data;

@Data
public class GetMessage {
    private String to;
    private Integer id;
}