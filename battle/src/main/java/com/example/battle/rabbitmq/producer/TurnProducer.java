package com.example.battle.rabbitmq.producer;

import com.example.battle.dto.TurnMessage;
import com.example.battle.rabbitmq.RabbitMQConnection;
import com.example.battle.utils.Partitioner;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class TurnProducer extends RabbitProducer<TurnMessage> {

    @Value("${rabbit.turn.routing-key}")
    private String routingKey;

    @Value("${rabbit.turn.exchange}")
    private String exchange;
    @Value("#{'${rabbit.turn.queue}'.split(',')}")
    private List<String> queues;


    public TurnProducer(RabbitMQConnection connection) {
        super(connection);
    }

    @Override
    public void declareQueue() throws Exception {
        try (Channel channel = rabbitMQConnection.getConnection().createChannel()) {
            channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT);
            for (int i = 0; i < queues.size(); i++) {
                channel.queueDeclare(queues.get(i), false, false, false, null);
                channel.queueBind(queues.get(i), exchange, String.format("%s-%s", routingKey, i));
            }

        }
    }

    public void pushMessage(TurnMessage turnMessage) {
        String id = turnMessage.getBattle_id();

        pushToQueue(new ProducerConfig.ExchangeConfig(exchange, BuiltinExchangeType.DIRECT), String.format("%s-%s", routingKey, Partitioner.partition(id, queues.size())), turnMessage);
    }

}
