package com.example.battle.rabbitmq.producer;

import com.rabbitmq.client.BuiltinExchangeType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class ProducerConfig {
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ExchangeConfig {
        private String name;
        private BuiltinExchangeType type;
    }
}
