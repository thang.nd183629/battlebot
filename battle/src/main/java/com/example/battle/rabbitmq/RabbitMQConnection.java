package com.example.battle.rabbitmq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RabbitMQConnection {
    private Connection connection;

    public RabbitMQConnection(@Value("${rabbit.host}") String host, @Value("${rabbit.username}") String username,@Value("${rabbit.password}") String password) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(username);
        factory.setPassword(password);
        connection = factory.newConnection();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                connection.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }));
    }

    public Connection getConnection() {
        return connection;
    }

}
