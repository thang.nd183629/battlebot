package com.example.battle.rabbitmq.consumer;

import com.example.battle.dto.TurnMessage;
import com.example.battle.rabbitmq.RabbitMQConnection;
import com.example.battle.service.TurnService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Slf4j
public class RabbitTurnConsumerThread extends RabbitConsumerThread<TurnMessage> {


    @Autowired
    private TurnService turnService;


    public RabbitTurnConsumerThread(String queue) {

        super(queue, TurnMessage.class);
    }

    @Override
    public void processMessage(TurnMessage body) {

        turnService.processTurn(body);
    }
//
//    @Override
//    public void run() {
//        try {
//            Connection connection = rabbitMQConnection.getConnection();
//            Channel channel = connection.createChannel();
//            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
//                TurnMessage message = SERIALIZER.readValue(delivery.getBody(), TurnMessage.class);
//                turnService.processTurn(message);
//                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), true);
//            };
//            CancelCallback cancelCallback = consumerTag -> {
//            };
//            channel.queueDeclare(queue, false, false, false, null);
//            channel.basicConsume(queue, false, deliverCallback, cancelCallback);
//
//
//        } catch (
//                Exception e) {
//            e.printStackTrace();
//            log.error(e.getMessage());
//        }
//
//
//    }
}
