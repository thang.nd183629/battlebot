package com.example.battle.rabbitmq.producer;

import com.example.battle.rabbitmq.RabbitMQConnection;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;


@Slf4j
public abstract class RabbitProducer<V> {
    protected RabbitMQConnection rabbitMQConnection;
    @Autowired
    private ObjectMapper SERIALIZER;


    public RabbitProducer(RabbitMQConnection connection) {
        this.rabbitMQConnection = connection;
    }

    public abstract void declareQueue() throws Exception;


    protected void pushToQueue(ProducerConfig.ExchangeConfig exchange, String routingKey, V body) {
        try {
            Connection connection = rabbitMQConnection.getConnection();
            try (Channel channel = connection.createChannel()) {
                declareQueue();
                channel.basicPublish(exchange.getName(), routingKey, null, SERIALIZER.writeValueAsBytes(body));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }


}
