package com.example.battle.rabbitmq.consumer;

import com.example.battle.rabbitmq.RabbitMQConnection;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;


@Slf4j
public abstract class RabbitConsumerThread<T> extends Thread {

    @Autowired
    protected  RabbitMQConnection rabbitMQConnection;

    private String queue;
    private Class<T> clazz;

    public abstract void processMessage(T body);

    public RabbitConsumerThread(String queue, Class<T> clazz) {

        super();

        this.queue = queue;
        this.clazz = clazz;
    }

    @Autowired
    private ObjectMapper SERIALIZER;

    @Override
    public void run() {
        try {
            Connection connection = rabbitMQConnection.getConnection();
            Channel channel = connection.createChannel();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                T message = SERIALIZER.readValue(delivery.getBody(), clazz);

                processMessage(message);
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), true);
            };
            CancelCallback cancelCallback = consumerTag -> {
            };
            channel.queueDeclare(queue, false, false, false, null);
            channel.basicConsume(queue, false, deliverCallback, cancelCallback);


        } catch (
                Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
