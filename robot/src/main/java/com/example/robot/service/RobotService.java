package com.example.robot.service;

import com.example.robot.model.Inventory;
import com.example.robot.model.Robot;
import com.example.robot.model.Token;
import com.example.robot.repository.InventoryRepository;
import com.example.robot.repository.RoboRepository;
import com.example.robot.repository.TokenRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import exception.ClientErrorException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import example.com.cache.ICache;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RobotService {

    private final RoboRepository roboRepository;
    private final InventoryRepository inventoryRepository;
    private final TokenRepository tokenRepository;
    private final ICache redisCache;

    @Value("${redis.robot.cache.prefix}")
    private String ROBOT_CACHE_PREFIX;
    @Value("${base.stat.incr}")
    private Integer baseStatUpgrade;
    @Value("${upgrade.cost}")
    private Integer upgradeCost;

    private final ObjectMapper SERIALIZER;

    public Robot getRobotById(String id) {
        try {
            String robotCached = redisCache.get(ROBOT_CACHE_PREFIX + id);
            Robot robot;
            if (robotCached == null) {
                robot = roboRepository.findById(id).orElseThrow(() -> new ClientErrorException("Robot not found", HttpStatus.NOT_FOUND));
                redisCache.set(ROBOT_CACHE_PREFIX + id, SERIALIZER.writeValueAsString(robot), 72000);
            } else {
                robot = SERIALIZER.readValue(robotCached, Robot.class);
            }


            return robot;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Robot> getAll() {
        return roboRepository.findAll();
    }


    public Robot buyRobot(String userId, String robotId) {
        Inventory inventory = inventoryRepository.findLatestPurchase(userId, robotId);
        Robot robot = getRobotById(robotId);
        if (inventory != null) {
            return buildRobot(inventory);
        }
        Token token = tokenRepository.findByUserId(userId);
        if (token == null) {
            token = new Token(null, userId, 0L);
        }
        if (robot.getCost() > token.getAmount()) {
            throw new ClientErrorException("Not enough token", HttpStatus.OK);
        }
        inventory = new Inventory();
        inventory.setRobot_id(robotId);
        inventory.setUser_id(userId);
        inventoryRepository.save(inventory);
        token.setAmount(token.getAmount() - robot.getCost());
        tokenRepository.save(token);
        return buildRobot(inventory);

    }

    public Robot getUserRobot(String userId, String robotId) {
        Inventory inventory = inventoryRepository.findLatestPurchase(userId, robotId);
        if (inventory == null) {
            throw new ClientErrorException("Robot not found", HttpStatus.NOT_FOUND);
        }
        return buildRobot(inventory);
    }

    private Robot buildRobot(Inventory inventory) {
        String id = inventory.getRobot_id();
        Robot robot = getRobotById(id);
        robot.setCurrentAtk(robot.getAtk() + baseStatUpgrade * inventory.getLevel());
        robot.setCurrentHp(robot.getHp() + baseStatUpgrade * inventory.getLevel());
        return robot;

    }

    public Robot upgrade(String userId, String robotId) {
        Inventory existedInventory = inventoryRepository.findLatestPurchase(userId, robotId);
        if (existedInventory == null) throw new ClientErrorException("Robot not found", HttpStatus.OK);
        Inventory inventory = new Inventory();
        inventory.setLevel(existedInventory.getLevel());
        inventory.setRobot_id(existedInventory.getRobot_id());
        inventory.setUser_id(existedInventory.getUser_id());
        Token token = tokenRepository.findByUserId(userId);

        if (token == null) {
            token = new Token(null, userId, 0L);
        }

        if (upgradeCost > token.getAmount()) {
            throw new ClientErrorException("Not enough token", HttpStatus.OK);
        }

        inventory.setLevel(inventory.getLevel() + 1);
        inventoryRepository.save(inventory);
        token.setAmount(token.getAmount() - upgradeCost);
        tokenRepository.save(token);
        return buildRobot(inventory);
    }


}
