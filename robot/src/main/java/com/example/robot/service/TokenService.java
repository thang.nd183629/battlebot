package com.example.robot.service;

import com.example.robot.model.Token;
import com.example.robot.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TokenService {
    @Autowired
    private TokenRepository tokenRepository;

    public Token updateToken(Long amount, String userId) {

        Token token = tokenRepository.findByUserId(userId);
        if (token == null) {
            token = new Token(null, userId, amount);
        } else {
            token.setAmount(token.getAmount() + amount);
        }
        tokenRepository.save(token);
        return token;


    }
}
