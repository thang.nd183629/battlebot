package com.example.robot.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import example.com.cache.ICache;
import example.com.cache.JedisCacheImpl;

@Configuration
public class ApplicationConfiguration {
    @Bean
    public ICache registerRedis(@Value("${redis.port}") Integer port, @Value("${redis.host}") String host) {
        return new JedisCacheImpl(port, host);
    }
}
