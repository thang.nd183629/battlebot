package com.example.robot.repository;


import com.example.robot.model.Robot;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoboRepository extends CrudRepository<Robot, String> {
    List<Robot> findAll();
}
