package com.example.robot.repository;

import com.example.robot.model.Token;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TokenRepository extends CrudRepository<Token, String> {
    Token findByUserId(String userId);
}
