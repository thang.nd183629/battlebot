package com.example.robot.repository;

import com.example.robot.model.Inventory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface InventoryRepository extends CrudRepository<Inventory, String> {
    @Query(nativeQuery = true, value = "select * from inventory where inventory.user_id = :userId and inventory.robot_id = :robotId order by level desc limit 1")
    Inventory findLatestPurchase(String userId, String robotId);
}
