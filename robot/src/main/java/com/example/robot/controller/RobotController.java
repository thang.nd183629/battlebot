package com.example.robot.controller;

import com.example.robot.dto.UpdateTokenDTO;
import com.example.robot.service.RobotService;
import com.example.robot.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class RobotController {

    private final RobotService robotService;
    @Autowired
    private TokenService tokenService;

    @GetMapping("/robot/{id}")
    public ResponseEntity<?> getRobotInfo(@PathVariable String id) {
        return ResponseEntity.ok(robotService.getRobotById(id));
    }

    @GetMapping("/robot")
    public ResponseEntity<?> getAllRobot() {
        return ResponseEntity.ok(robotService.getAll());
    }

    @GetMapping("/user/robot/{id}")
    public ResponseEntity<?> getUserInventory(@RequestHeader("user-id") String userId, @PathVariable(name = "id") String robotId) {
        return ResponseEntity.ok(robotService.getUserRobot(userId, robotId));
    }

    @PutMapping("/user/robot/{id}/upgrade")
    public ResponseEntity<?> upgradeRobot(@RequestHeader("user-id") String userId, @PathVariable(name = "id") String robotId) {
        return ResponseEntity.ok(robotService.upgrade(userId, robotId));
    }

    @PostMapping("/user/robot/{id}")
    public ResponseEntity<?> buyRobot(@RequestHeader("user-id") String userId, @PathVariable(name = "id") String robotId) {
        return ResponseEntity.ok(robotService.buyRobot(userId, robotId));
    }

    @PostMapping("/user/token")
    public ResponseEntity<?> updateToken(@RequestHeader("user-id") String userId, @RequestBody UpdateTokenDTO payload) {
        return ResponseEntity.ok(
                tokenService.updateToken(payload.getAmount(), userId)
        );
    }
}