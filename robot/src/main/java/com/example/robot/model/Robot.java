package com.example.robot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Robot {


    @Id
    @Column(name = "id", nullable = false)
    private String id;

    private Integer hp;
    private Integer atk;
    private RoboType type;
    private Integer cost;

    @Transient
    private Integer currentHp;
    @Transient
    private Integer currentAtk;


}
