package com.example.robot.model;

public enum RoboType {
    RAIDER, LAVA, OBLIX
}
