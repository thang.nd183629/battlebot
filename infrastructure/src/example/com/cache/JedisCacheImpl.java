package example.com.cache;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.params.SetParams;


public class JedisCacheImpl implements ICache {
    private JedisPool connectionPool;


    public JedisCacheImpl(
            Integer port,
            String host) {
        JedisPoolConfig poolConfig = new JedisPoolConfig();

        poolConfig.setMaxTotal(10000);
        poolConfig.setMaxIdle(500);

        poolConfig.setTestWhileIdle(true);
        poolConfig.setTestOnBorrow(false);
        poolConfig.setTestOnReturn(false);

        connectionPool = new JedisPool(poolConfig, host, port, Protocol.DEFAULT_TIMEOUT);

    }


    @Override
    public Boolean set(String key, String value, Integer expired) {
        try (Jedis jedis = connectionPool.getResource()) {

            jedis.set(key, value);
            if (expired != null) {
                jedis.expire(key, Long.valueOf(expired));
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean del(String key) {
        try (Jedis jedis = connectionPool.getResource()) {
            jedis.del(key);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public String get(String key) {
        try (Jedis jedis = connectionPool.getResource()) {
            return jedis.get(key);
        } catch (Exception e) {
            return null;
        }

    }

}
