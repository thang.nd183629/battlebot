package example.com.cache;

public interface ICache {
    Boolean set(String key, String value, Integer expired);
    Boolean del(String key);
    String get(String key);


}
